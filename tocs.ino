//#include <Adafruit_LSM6DSOX.h>
//Adafruit_LSM6DSOX lsm6ds;

// To use with the LSM6DS33+LIS3MDL breakout, uncomment these two lines
// and comment out the lines referring to the LSM6DSOX above
//#include <Adafruit_LSM6DS33.h>
//Adafruit_LSM6DS33 lsm6ds;

// To use with the ISM330DHCX+LIS3MDL Feather Wing, uncomment these two lines
// and comment out the lines referring to the LSM6DSOX above
#include <Adafruit_ISM330DHCX.h>
Adafruit_ISM330DHCX lsm6ds;

#include <Adafruit_LIS3MDL.h>
Adafruit_LIS3MDL lis3mdl;

#include <SPI.h>
#include <SD.h>
#include "RTClib.h"


File fileCattura;
RTC_DS1307 rtc;

void setup(void) {
	Serial.begin(115200);    

	Serial.println("[INFO] Initializing SD card");
	if (!SD.begin(10)) {
		Serial.println("[ERROR] Initialization failed!");
		while (1);
	}    
	Serial.println("[INFO] Initialization done.");    

	if (!rtc.begin()) {
		Serial.println("Couldn't find RTC");
		while (1);
	}

	if (! rtc.isrunning()) {
		Serial.println("RTC is NOT running!");
	}
	else {
		// following line sets the RTC to the date & time this sketch was compiled
		rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
	}

	bool lsm6ds_success, lis3mdl_success;

	lsm6ds_success = lsm6ds.begin_I2C();
	lis3mdl_success = lis3mdl.begin_I2C();

	if (!lsm6ds_success){
		Serial.println("Failed to find LSM6DS chip");
	}
	if (!lis3mdl_success){
		Serial.println("Failed to find LIS3MDL chip");
	}
	if (!(lsm6ds_success && lis3mdl_success)) {
		while (1) {
			delay(10);
		}
	}

	lis3mdl.setDataRate(LIS3MDL_DATARATE_155_HZ);

	lis3mdl.setRange(LIS3MDL_RANGE_4_GAUSS);

	lis3mdl.setPerformanceMode(LIS3MDL_MEDIUMMODE);

	lis3mdl.setOperationMode(LIS3MDL_CONTINUOUSMODE);

	lis3mdl.setIntThreshold(500);
	lis3mdl.configInterrupt(false, false, true, // enable z axis
			true, // polarity
			false, // don't latch
			true); // enabled!
}

void loop() {
	sensors_event_t accel, gyro, mag, temp;

	//  /* Get new normalized sensor events */
	lsm6ds.getEvent(&accel, &gyro, &temp);
	lis3mdl.getEvent(&mag);

	fileCattura = SD.open("cattura.csv", FILE_WRITE);

	DateTime now = rtc.now();

	// TIME  
	Serial.print(now.year(), DEC);
	Serial.print('-');
	Serial.print(now.month(), DEC);
	Serial.print('-');
	Serial.print(now.day(), DEC);
	Serial.print(" ");
	Serial.print(now.hour(), DEC);
	Serial.print(':');
	Serial.print(now.minute(), DEC);
	Serial.print(':');
	Serial.print(now.second(), DEC);
	Serial.print(",");

	// ACCELERATION
	Serial.print(accel.acceleration.x, 4); // X
	Serial.print(",");
	Serial.print(accel.acceleration.y, 4); // Y
	Serial.print(",");
	Serial.print(accel.acceleration.z, 4); // Z
	Serial.print(",");

	// GYROSCOPE
	Serial.print(gyro.gyro.x, 4); // X
	Serial.print(",");
	Serial.print(gyro.gyro.y, 4); // Y
	Serial.print(",");
	Serial.print(gyro.gyro.z, 4); // rotation in rad/s
	Serial.print(",");

	// MAGNETIC FIELD
	Serial.print(mag.magnetic.x, 4); // X
	Serial.print(",");
	Serial.print(mag.magnetic.y, 4); // Y
	Serial.print(",");
	Serial.print(mag.magnetic.z, 4); // in µTesla
	Serial.print(",");

	Serial.print(temp.temperature); // Temperature in deg. Celsius
	Serial.println();

	if (fileCattura) {
		fileCattura.print(now.year(), DEC);
		fileCattura.print('-');
		fileCattura.print(now.month(), DEC);
		fileCattura.print('-');
		fileCattura.print(now.day(), DEC);
		fileCattura.print(" ");
		fileCattura.print(now.hour(), DEC);
		fileCattura.print(':');
		fileCattura.print(now.minute(), DEC);
		fileCattura.print(':');
		fileCattura.print(now.second(), DEC);
		fileCattura.print(",");  

		fileCattura.print(accel.acceleration.x, 4); // X)
		fileCattura.print(",");
		fileCattura.print(accel.acceleration.y, 4); // Y)
		fileCattura.print(",");
		fileCattura.print(accel.acceleration.z, 4); // Z)
		fileCattura.print(",");

		fileCattura.print(gyro.gyro.x, 4); // X
		fileCattura.print(",");
		fileCattura.print(gyro.gyro.y, 4); // Y
		fileCattura.print(",");
		fileCattura.print(gyro.gyro.z, 4); // rotation in rad/s
		fileCattura.print(",");

		fileCattura.print(mag.magnetic.x, 4); // X
		fileCattura.print(",");
		fileCattura.print(mag.magnetic.y, 4); // Y
		fileCattura.print(",");
		fileCattura.print(mag.magnetic.z, 4); // in µTesla
		fileCattura.print(",");	

		fileCattura.print(temp.temperature); // Temperature in deg. Celsius
		fileCattura.println();	

		fileCattura.close();    
	} 

	else {
		Serial.println("Errore nell'apertura del file.");
	}

	delay(2000);
}

// CAMPI FILE CSV:
//   yyyy-MM-dd HH:mm:ss TIME
//   x,y,z ACCEL
//   x,y,z GYRO
//   x,y,z MAGNETIC
//   temp TEMPERATURE
//  ( 11 campi totali: 'yyyy-MM-dd HH:mm:ss,x,y,z,x,y,z,x,y,z,temp' )
