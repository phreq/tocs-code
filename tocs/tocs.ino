#include <SD.h>
#include "RTClib.h"

// To use with the ISM330DHCX+LIS3MDL Feather Wing, uncomment these two lines
// and comment out the lines referring to the LSM6DSOX above
#include <Adafruit_ISM330DHCX.h>
Adafruit_ISM330DHCX lsm6ds;

File fileCattura;
RTC_DS1307 rtc;

void setup(void) {
	Serial.begin(115200);    

  bool lsm6ds_success;

  lsm6ds_success = lsm6ds.begin_I2C();

  if (!lsm6ds_success){
    Serial.println("Failed to find LSM6DS chip");
  }
  if (!(lsm6ds_success)) {
    while (1) {
      delay(10);
    }
  }

	Serial.println("[INFO] Initializing SD card");
	if (!SD.begin(10)) {
		Serial.println("[ERROR] Initialization failed!");
		while (1);
	}    
	Serial.println("[INFO] Initialization done.");    

	if (!rtc.begin()) {
		Serial.println("Couldn't find RTC");
		while (1);
	}

	if (!rtc.isrunning()) {
		Serial.println("RTC is NOT running!");
		// following line sets the RTC to the date & time this sketch was compiled
		rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
	}
}

void loop() {
	sensors_event_t accel, gyro, mag, temp;

	// Get new normalized sensor events
	lsm6ds.getEvent(&accel, &gyro, &temp);

	fileCattura = SD.open("cattura.csv", FILE_WRITE);

	DateTime now = rtc.now();

	// TIME  
	Serial.print(now.year(), DEC);
	Serial.print('-');
	Serial.print(now.month(), DEC);
	Serial.print('-');
	Serial.print(now.day(), DEC);
	Serial.print(" ");
	Serial.print(now.hour(), DEC);
	Serial.print(':');
	Serial.print(now.minute(), DEC);
	Serial.print(':');
	Serial.print(now.second(), DEC);
	Serial.print(",");

	// ACCELERATION
	Serial.print(accel.acceleration.x, 4); // X
	Serial.print(",");
	Serial.print(accel.acceleration.y, 4); // Y
	Serial.print(",");
	Serial.print(accel.acceleration.z, 4); // Z
	Serial.print(",");

	// GYROSCOPE
	Serial.print(gyro.gyro.x, 4); // X
	Serial.print(",");
	Serial.print(gyro.gyro.y, 4); // Y
	Serial.print(",");
	Serial.print(gyro.gyro.z, 4); // rotation in rad/s
	Serial.print(",");

	Serial.print(temp.temperature); // Temperature in deg. Celsius
	Serial.println();

	if (fileCattura) {
		fileCattura.print(now.year(), DEC);
		fileCattura.print('-');
		fileCattura.print(now.month(), DEC);
		fileCattura.print('-');
		fileCattura.print(now.day(), DEC);
		fileCattura.print(" ");
		fileCattura.print(now.hour(), DEC);
		fileCattura.print(':');
		fileCattura.print(now.minute(), DEC);
		fileCattura.print(':');
		fileCattura.print(now.second(), DEC);
		fileCattura.print(",");  

		fileCattura.print(accel.acceleration.x, 4); // X)
		fileCattura.print(",");
		fileCattura.print(accel.acceleration.y, 4); // Y)
		fileCattura.print(",");
		fileCattura.print(accel.acceleration.z, 4); // Z)
		fileCattura.print(",");

		fileCattura.print(gyro.gyro.x, 4); // X
		fileCattura.print(",");
		fileCattura.print(gyro.gyro.y, 4); // Y
		fileCattura.print(",");
		fileCattura.print(gyro.gyro.z, 4); // rotation in rad/s
		fileCattura.print(",");

		fileCattura.print(temp.temperature); // Temperature in deg. Celsius
		fileCattura.println();	

		fileCattura.close();    
	} 

	else {
		Serial.println("Errore nell'apertura del file.");
	}

	delay(20);
}

// CAMPI FILE CSV:
//   yyyy-MM-dd HH:mm:ss TIME
//   x,y,z ACCEL
//   x,y,z GYRO
//   temp TEMPERATURE
//  ( 8 campi totali: 'yyyy-MM-dd HH:mm:ss,x,y,z,x,y,z,temp' )
